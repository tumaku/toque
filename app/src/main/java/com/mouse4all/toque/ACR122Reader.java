/*
 * Copyright 2018 Mouse4all SL
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mouse4all.toque;

import java.util.HashMap;
import java.util.Iterator;

import com.acs.smartcard.Reader;
import com.acs.smartcard.ReaderException;

import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


public class ACR122Reader {
	public final static String LOG_TAG="ACR122 Reader";
	public final static int VENDORID_ACR122 []= {1839};

	
	private static Reader reader=null;	
	private static UsbDevice device=null;
    private static Context context=null;

    public static void destroyReader(){
    	closeReader();
    	device=null;
    	//context= null;
    }
      
    public static void openReader(Context c) {
    	context =c;
		UsbManager usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
		closeReader();

        reader = new Reader(usbManager);           	
        reader.setOnStateChangeListener(new Reader.OnStateChangeListener() {
            @Override
            public void onStateChange(int slotNumber, int previousState, int currentState) {
            	Log.d(LOG_TAG, "State change. Slot: " + slotNumber + "; Prev: " + previousState +
            			"; Current: " + currentState);
            	String tmpLog="\n";
            	if (currentState==Reader.CARD_ABSENT) {
            		tmpLog+="Card removed.";
            		Log.d(LOG_TAG, "Card removed");
            		//broadcastIntent(tmpLog);
            	}
            	if ((previousState==Reader.CARD_ABSENT)&&(currentState==Reader.CARD_PRESENT)) {
            		try {
            			byte [] atr= reader.getAtr(slotNumber);
            			String atrString="";
            			if (atr==null) {
            				atrString="Card has no ATR";
            			} else {
                            for (int i=0; i<atr.length;i++) {
                            	atrString+=String.format("%02X", atr[i]);
                            }
            			}
            			tmpLog+= "Reader ATR: " +atrString;
            			tmpLog+= "\nReader State A: " +reader.getState(slotNumber);
            			reader.power(slotNumber, Reader.CARD_WARM_RESET);
            			tmpLog+= "\nReader State B: " +reader.getState(slotNumber);
            			reader.setProtocol(slotNumber, Reader.PROTOCOL_T0);
            			tmpLog+= "\nReader State C: " +reader.getState(slotNumber);
            			

            			
            			//byte[] readCommand = {(byte)0xFF,(byte)0xB0, 0x00, 0x00, 0x04 };
            			byte[] readUIDCommand = {(byte)0xFF,(byte)0xCA, 0x00, 0x00, 0x00 };
            			byte[] response = new byte[300];
            			int responseLength;
                        try {
                            responseLength = reader.transmit(slotNumber, readUIDCommand,
                            		readUIDCommand.length, response, response.length);
                            String text="";
                            for (int i=0; i<responseLength;i++) {
                            	text+=String.format("%02X", response[i]);
                            }
                            tmpLog+=text;
                            if ((responseLength>2)&&(text.endsWith("9000"))) {
                            	text=text.substring(0, text.length()-4);
        		            	Log.d(LOG_TAG, "New tag datected. UID: " + text);
                    		    broadcastIntent(text);
        					} else {
        		            	Log.d(LOG_TAG, "Error reading tag UID. " + tmpLog);
        					}
                        } catch (ReaderException e) {
                        	tmpLog+="Exception on APDU exchange: " + e.getMessage();
                        } 
             		} catch (Exception ex) {
            			tmpLog+= "\nException while reading card: " + ex.getMessage();
            		}
            	}
            	tmpLog="State change. Slot: " + slotNumber + " - States: " +
            			previousState + " / " + currentState + tmpLog;
            	Log.d(LOG_TAG, tmpLog);
            }
        }); //onStateChange()

        if (checkUSB()) {
        	reader.open(device);
    	    byte[] beepingLedCommand = {(byte)0xFF,(byte)0x00, (byte)0x40, (byte)0xD0, (byte)0x04, (byte)0x05,(byte)0x05, (byte)0x03, (byte)0x01};
    	    byte[] buzzerOffCommand = {(byte)0xFF,(byte)0x00, (byte)0x52, (byte)0x00, (byte)0x00};
            try {
        		int responseLength;
        		byte[] response = new byte[300];
             //   responseLength = reader.control(0, Reader.IOCTL_CCID_ESCAPE, beepingLedCommand,
             //   		beepingLedCommand.length, response, response.length);            
                responseLength = reader.control(0, Reader.IOCTL_CCID_ESCAPE, buzzerOffCommand,
                		buzzerOffCommand.length, response, response.length);
            } catch (ReaderException e) {
           } 
        } else {
        	device=null;
        	Log.d(LOG_TAG, "Could not find an authorised rfid reader device");
        }
    
    }
    
    static void closeReader() {
    	if (device==null) return;
		UsbManager usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
		if (!usbManager.getDeviceList().containsValue(device)) return;

    	try{
    		if (reader!=null) {
        	    byte[] buzzerOnCommand = {(byte)0xFF,(byte)0x00, (byte)0x52, (byte)0xFF, (byte)0x00};
                try {
            		int responseLength;
            		byte[] response = new byte[300];
                 //   responseLength = reader.control(0, Reader.IOCTL_CCID_ESCAPE, beepingLedCommand,
                 //   		beepingLedCommand.length, response, response.length);            
                    responseLength = reader.control(0, Reader.IOCTL_CCID_ESCAPE, buzzerOnCommand,
                    		buzzerOnCommand.length, response, response.length);
                } catch (ReaderException e) {
               }     			
    		   reader.close();
    		}
    	} catch (Exception ex){
    		Log.d(LOG_TAG, "Error trying to close non-null reader");
    	}   	
    	reader= null;
    }
    
    
    static boolean checkUSB(){
    	UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
 		// Get the list of attached devices
		HashMap<String, UsbDevice> devices = manager.getDeviceList();		
		// Iterate over all devices
		Iterator<String> it = devices.keySet().iterator();
		while (it.hasNext()) {
			String deviceName = it.next();	
			UsbDevice dev = devices.get(deviceName);
			String VID = Integer.toHexString(dev.getVendorId()).toUpperCase();
			String PID = Integer.toHexString(dev.getProductId()).toUpperCase();
			Log.d(LOG_TAG,deviceName + " " +  VID + ":" + PID + " " + manager.hasPermission(dev));
			if (!isRfidReaderDevice(dev.getVendorId())) {
				Log.d(LOG_TAG, "Not an Rfid reader device. Keep looking");
			    break;
			}
			if (!manager.hasPermission(dev)) {
         	    MainActivity.showToast(context.getString(R.string.permissionDenied));
				Log.d(LOG_TAG, "Requesting user authorisation to access USB device");
				return false;
			} else {
				device=dev;
                return true;
			}
		}
		return false;
    }

    public static boolean isRfidReaderDevice(int deviceVID){
 	   for (int target : VENDORID_ACR122) {
  	       if (deviceVID==target) {
 		    	      Log.d(LOG_TAG, "Detected ACR reader device!");	
 		    	      return true;
  	       }    	    	   
 	   }   	
 	   return false;
	}
	    	
    private static void broadcastIntent(String extra) {
    	boolean cardManagementFlag = Repository.getConfiguration(context, context.getString(R.string.cardManagementFlag));
    			
    	if (cardManagementFlag){
	        Intent intent = new Intent();
	        intent.setClass(context, CardManagementActivity.class);
	        intent.putExtra(context.getString(R.string.TAGID_EXTRA), extra);
	        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
	        context.startActivity(intent);					    		
    	} else {
	    	Log.d(LOG_TAG,"Local Broadcast event action");
	    	Intent intent = new Intent();            	
			intent.setAction(context.getString(R.string.cardEventServiceAction));
			intent.putExtra(context.getResources().getString(R.string.cardEventExtra), extra);
			LocalBroadcastManager lBM=LocalBroadcastManager.getInstance(context);
			lBM.sendBroadcast(intent); 		            		  
    	}
   }
}