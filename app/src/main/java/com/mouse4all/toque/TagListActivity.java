/*
 * Copyright 2018 Mouse4all SL
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mouse4all.toque;

import java.util.List;

import com.mouse4all.toque.db.DatabaseHandler;
import com.mouse4all.toque.db.RfidTag;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class TagListActivity extends ListActivity {
	private static final String LOG_TAG="ACR122 TagListActivity";
	
	private Context context;
    private Button okButton;
    private List<RfidTag> listValues;
	private DatabaseHandler db;
	private RfidCardAdapter rfidCardAdapter;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        //setTheme(android.R.style.Theme_DeviceDefault_Light);
        setContentView(R.layout.activity_tag_list);
		okButton= (Button) findViewById(R.id.okButton);
		okButton.setOnClickListener(new OnClickListener() {          
			public void onClick(View v) {
				((Activity)context).finish();
			}
		});	
        initialiseTagList();
    }
    
    private void formatHeaderField(TextView view) {
    	Configuration config= getResources().getConfiguration() ;
    	if ((config.screenWidthDp<500)||(config.screenHeightDp<500)){
    		view.setTextAppearance(this, android.R.style.TextAppearance_Medium);
    	} else {
    		view.setTextAppearance(this, android.R.style.TextAppearance_Large);    		
    	}
    	view.setTypeface(null, Typeface.BOLD);    	
    	view.setTextColor(Color.WHITE);
    }
    
    private void initialiseTagList(){
		db=new DatabaseHandler(this);
		listValues= db.getAllRfidTags();
	    rfidCardAdapter = new RfidCardAdapter(this, listValues);
	     
		
	    LinearLayout rowLayout=(LinearLayout) findViewById(R.id.headerLayout);
	    rowLayout.setBackgroundColor(Color.BLACK);	    
	    
	    TextView tagIdText= (TextView)findViewById(R.id.tagId);
	    TextView aliasText=(TextView) findViewById(R.id.alias);
	    TextView taskText=(TextView) findViewById(R.id.task);
	    tagIdText.setText(getString(R.string.tagId));
	    formatHeaderField(tagIdText);
	    aliasText.setText(getString(R.string.alias));
	    formatHeaderField(aliasText);
	    taskText.setText(getString(R.string.taskName));
	    formatHeaderField(taskText);
	    setListAdapter(rfidCardAdapter);
	    //this.getListView().setScrollbarFadingEnabled(false);
	    //this.getListView().setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
    }
    
    @Override
    protected void onNewIntent(Intent intent) {
    	// Activity defined in manifest as single instance, so when the activity is already created,
    	// new intents arrive through this method, refresh the tag list just in case CardManagement activity
    	// modified it.
        super.onNewIntent(intent);
        initialiseTagList();
    }
     
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.tag_list_menu, menu);
        return true;
    }
  
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {        
        switch (item.getItemId())
        {
	        case R.id.reset_tag_list:
                new AlertDialog.Builder(context)
                .setTitle(getString(R.string.resetTagList))
                .setMessage(getString(R.string.deleteAllTagsText))
                .setNegativeButton(getString(R.string.cancel),null)
                .setPositiveButton(getString(R.string.delete), 
                	new DialogInterface.OnClickListener() {
			            @Override
			            public void onClick(DialogInterface dialog, int which) {
							db.onUpgrade(db.getWritableDatabase(), 0, 0);
							rfidCardAdapter.clear();
			            }
        			})	
                .show();
	        	
	            return true;

			case R.id.create_dummy_tag_list:
				new AlertDialog.Builder(context)
						.setTitle(getString(R.string.dummyTagList))
						.setMessage(getString(R.string.dummyTagListText))
						.setNegativeButton(getString(R.string.cancel),null)
						.setPositiveButton(getString(R.string.createDummyList),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										db.onUpgrade(db.getWritableDatabase(), 0, 0);
										rfidCardAdapter.clear();
										for (int i=0; i<100;i++){
											db.addRfidTag(new RfidTag(Integer.toString(i)+"Tag",
													"Alias"+ Integer.toString(i), "---"));
										}
									}
								})
						.show();

				return true;

	        default:
	            return super.onOptionsItemSelected(item);
        }
    }    
    
    @Override
    protected void onListItemClick(ListView list, View view, int position, long id) {
        super.onListItemClick(list, view, position, id);
        RfidTag selectedTag = (RfidTag) getListView().getItemAtPosition(position);
        RfidTagDialog.showUserDialog(context, selectedTag, rfidCardAdapter);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }
    
    @Override
    protected void onPause() {
        super.onPause();
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	Log.d(LOG_TAG, "Activity result received from Tasker select task. ResultCode: " + resultCode);
    	if (resultCode == Activity.RESULT_OK && requestCode == RfidTagDialog.TASKER_REQUEST_CODE) {
		    String taskName = data.getDataString();
		    if ((taskName!=null)) {
		    	RfidTagDialog.refreshTaskName(this, taskName);
		    }
		} else {
			Log.d(LOG_TAG, "Activity result received from Tasker select task. RfidTag not updated. ResultCode: " + resultCode);											
		}
    }    
}
