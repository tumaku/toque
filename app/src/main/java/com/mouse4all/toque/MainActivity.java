/*
 * Copyright 2018 Mouse4all SL
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mouse4all.toque;

import java.util.HashMap;
import java.util.Iterator;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private static final String LOG_TAG="ACR122 Main ";

	TextView connectText;
	
	Context context;
	
    private final BroadcastReceiver broadcastReceiver = new MainActivityBroadcastReceiver(this);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		context =this;
		super.onCreate(savedInstanceState);
        //setTheme(android.R.style.Theme_DeviceDefault_Light);
		setContentView(R.layout.activity_main);
 		connectText = (TextView) findViewById(R.id.connectText);
    	Configuration config= getResources().getConfiguration() ;
 	   	if ((config.screenWidthDp<500)||(config.screenHeightDp<500)){
 	   		connectText.setTextAppearance(this, android.R.style.TextAppearance_Medium);
    	} else {
    		connectText.setTextAppearance(this, android.R.style.TextAppearance_Large);    		
    	}
		//showToast("MainActivity onCreate");
        checkIntent(getIntent());   
	}

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(context.getString(R.string.ACTION_USB_PERMISSION));
        registerReceiver(broadcastReceiver, filter);
    }
    
    @Override
    protected void onPause() {
        super.onPause();
	    unregisterReceiver(broadcastReceiver);
   }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
    	// Activity defined in manifest as single instance, so when the activity is already created,
    	// new intents arrive through this method
        super.onNewIntent(intent);
		//showToast("MainActivity onNewIntent");
        checkIntent(intent);
    }
    
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.menu_card_list) {
			startActivity(TagListActivity.class);
			return true;
		}
		if (id == R.id.menu_help) {
			showHelp(this);
			return true;
		}
		if (id == R.id.menu_about) {
			showAbout(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void startActivity(Class className){
        Intent intent = new Intent();
        intent.setClass(context, className);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(intent);	            		
	}
	  
	   
	   
	public static void showToast(String msg){
		showToast(msg, Toast.LENGTH_SHORT);
	}
   
	public static void showToast(String msg, int length){
	    Toast toast=Toast.makeText( AppContainer.getInstance(), msg, length);
	    LinearLayout toastLayout = (LinearLayout) toast.getView();
	    toastLayout.setBackgroundColor(Color.parseColor("#FF5722"));
	    //TextView toastTV = (TextView) toastLayout.getChildAt(0);
	    //toastTV.setTextSize(30);
	    toast.show(); 
	}


	public static void showHelp(Context cont) {
		TextView msg = new TextView(cont);
		msg.setText(Html.fromHtml(cont.getString(R.string.helpMessage)));
		msg.setPadding(10, 5,10, 5);
		msg.setMovementMethod(LinkMovementMethod.getInstance());

		AlertDialog aboutDialog= new AlertDialog.Builder(cont)
				.setTitle(cont.getString(R.string.help))
				.setIcon(R.drawable.ic_launcher)
				.setView(msg)
				.setNegativeButton(cont.getString(R.string.ok), null).show();
	}


	public static void showAbout(Context cont) {
		TextView msg = new TextView(cont);
		String versionName="";
		try {
			versionName= cont.getPackageManager()
					.getPackageInfo(cont.getPackageName(), 0).versionName;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		String tmp= String.format(
				cont.getString(R.string.aboutMessage),
				versionName);
		msg.setText(Html.fromHtml(tmp));
		msg.setPadding(10, 5,10, 5);
		msg.setMovementMethod(LinkMovementMethod.getInstance());

		AlertDialog aboutDialog= new AlertDialog.Builder(cont)
				.setTitle(cont.getString(R.string.app_name))
				.setIcon(R.drawable.ic_launcher)
				.setView(msg)
				.setNegativeButton(cont.getString(R.string.ok), null).show();
	}

	void checkIntent(Intent i) {
    	// If screen orientation is changed, the activity is destroyed and recreated but the
    	// last intent is used to create the new instance
    	// Unrealistic scenario, but it may happen:
    	// 1.a Portrait mode: connect reader
    	// 1.b System UI asks for permission and activity triggered with USB_ATTACHED action
    	// 2. Disconnect reader
    	// 2.b if broadcast receiver active, USB_DETACHED intent received
    	// 3. Change to landscape orientation and connect reader before the checkIntent() method is called 
    	//    (easy if in debug mode and a breakpoint is set)
    	// 4. The received intent is a replay of the 1.b (action USB_ATTACHED) 
    	//    (although device has not received permission from user yet:-(
    	// To avoid this scenario we checkPermission even when triggered with USB_ATTACHED action
    	// If we did not check it, we may generate a security exception.
    	
    	Log.d(LOG_TAG, "CheckIntent");
    	if (i==null) {
    		Log.d(LOG_TAG, "Intent null");
     	    return;
    	}
		String action = i.getAction();
		if ((UsbManager.ACTION_USB_DEVICE_ATTACHED).equalsIgnoreCase(action)) {
			//triggered by new connected USB device
			Log.d(LOG_TAG, "USB device detected; it should already have user permission");
            UsbDevice device = (UsbDevice)i.getParcelableExtra(UsbManager.EXTRA_DEVICE);
            if (device==null){
            	Log.d(LOG_TAG, "No device");
         	    return;
            } 
	    	UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
	    	if ( manager ==null) {
	    	   //Intent should not have arrived
	           Log.d(LOG_TAG, "No USB support in this Android device.");               
	           return;	                		  
	    	}
	    	HashMap<String, UsbDevice> devices = manager.getDeviceList();
	    	if (devices.isEmpty()) {
	           Log.d(LOG_TAG, "No USB device connected but probably old intent re-received");               
	           return;
	    	}
	    	Log.d(LOG_TAG, "Device: " + device);
	    	if (manager.hasPermission(device)) { 
	           Log.d(LOG_TAG, "Permission granted for device. Start Reader Service");
	           if (ACR122Reader.isRfidReaderDevice(device.getVendorId())) {
	          	   startReaderService();
	         	    ((Activity)context).finish();
	           } else {
	           	   Log.d(LOG_TAG, "Not rfid reader");
	           }
	           return;
	    	}
	        //unlikely scenario described at the top of the method
	        Log.d(LOG_TAG, "Permission denied for device");
   	        checkUSB(false);  
	        return;
	    }

		if ( (i.hasExtra(getResources().getString(R.string.stop))) && 
      			 (i.hasExtra(getResources().getString(R.string.service_name_reader))) ) {
				    //Triggered by user through notification intent created by service.
  			Log.d(LOG_TAG, "User wants to stop Reader Service from STOP notification");
  			Intent stopIntent = new Intent();
  			stopIntent.setClass(context, ReaderService.class);
  	        AppContainer.getInstance().stopService(stopIntent);      
  			ReaderService.createRestartNotification();
        	((Activity)context).finish();   
        	return;
   		}
    		
		if  (i.hasExtra(getResources().getString(R.string.extra_restart))){
			//Triggered by user through notification intent created when service is stopped.
  			Log.d(LOG_TAG, "User started Reader Service from STOP notification of the foreground service");
			checkUSB(false);
			return;		
		}
    		
		Log.d(LOG_TAG, "Activity started manually by user from Android icon");
		checkUSB(true);
        return;
    }
    
    boolean checkUSB(boolean closeActivity){
    	// http://stackoverflow.com/questions/12388914/usb-device-access-pop-up-supression
    	UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
    	if (manager==null) {
    		Log.d(LOG_TAG,"Android device does not support USB OTG functionality");
        	return false;
    	}

    	HashMap<String, UsbDevice> devices = manager.getDeviceList();
		Iterator<String> it = devices.keySet().iterator();
		while (it.hasNext()) {
			String deviceName = it.next();
			UsbDevice device = devices.get(deviceName);			
			if (!ACR122Reader.isRfidReaderDevice(device.getVendorId())) {
				Log.d(LOG_TAG, "Not an Rfid reader device. Keep looking");
			    break;
			}
			if (!manager.hasPermission(device)) {
	        	PendingIntent mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(context.getString(R.string.ACTION_USB_PERMISSION)), 0);
				manager.requestPermission(device, mPermissionIntent);
				Log.d(LOG_TAG, "checkUSB requesting user authorisation to access USB device");
				return false;
			} else {
				Log.d(LOG_TAG, "checkUSB detected authorised ACR122 reader. Launching reader service.");
         	    startReaderService();
         	    if (closeActivity) {
         	    	startActivity(CardManagementActivity.class);
         	    }
         	    ((Activity)context).finish();
                return true;
			}
		}
		Log.d(LOG_TAG, "checkUSB No ACR122 reader connected");
		return false;
    }
    
    protected void startReaderService(){
    	if (isServiceRunning()){
    		Log.d(LOG_TAG, "Reader service already running");  
    		return;
    	}
		Intent i = new Intent();
		Log.d(LOG_TAG,  "Start service: " + (ReaderService.class).getName() );
		i.setClass(context, ReaderService.class);
		context.startService(i);    
	}
    
    
    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
    	Log.d(LOG_TAG, "Looking for Reader service: " + ReaderService.class.getName());            	
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (ReaderService.class.getName().contentEquals(service.service.getClassName())) {
            	Log.d(LOG_TAG, "Reader service name found");            	
            	if (service.service.getPackageName().contentEquals(getApplicationContext().getPackageName())) {
            		Log.d(LOG_TAG, "Package name also matches");
            		 return true;    
            	}
            	else Log.d(LOG_TAG, "Package name does not match");
            }
        }
    	Log.d(LOG_TAG, "Reader service not found :-(");            	
        return false;
    }
}
