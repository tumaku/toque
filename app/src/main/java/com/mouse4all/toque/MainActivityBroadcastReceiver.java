/*
 * Copyright 2018 Mouse4all SL
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mouse4all.toque;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

public class MainActivityBroadcastReceiver extends BroadcastReceiver {
	private static final String LOG_TAG="ACR122 MainActivity BR";
	private MainActivity mainActivity;
	
	public MainActivityBroadcastReceiver(MainActivity mainAct) {
		super();
		mainActivity=mainAct;
	}

	@Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(LOG_TAG, "Intent received by BR. Action: " + action);

        if (context.getString(R.string.ACTION_USB_PERMISSION).equals(action)) {
                UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                String deviceName = (device!=null)?device.getDeviceName():"NULL device";
                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                       	String text1 = "Permission granted for " + deviceName;
                    	mainActivity.startReaderService();
                        Log.d(LOG_TAG, text1);   
                } else {
                	String text = context.getString(R.string.permissionDenied, deviceName);
                	MainActivity.showToast(text);
                    Log.d(LOG_TAG, text);
                }
        } 
    }

}
