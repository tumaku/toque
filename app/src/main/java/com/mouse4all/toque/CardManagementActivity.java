/*
 * Copyright 2018 Mouse4all SL
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mouse4all.toque;

import com.mouse4all.toque.db.DatabaseHandler;
import com.mouse4all.toque.db.RfidTag;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class CardManagementActivity extends Activity {
	private static final String LOG_TAG="ACR122 CardMngmnt";

	private DatabaseHandler db;
    Button okButton;
	TextView cardManagementText;
	Context contextActivity;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		contextActivity=this;
		db=new DatabaseHandler(contextActivity);
		setContentView(R.layout.activity_card_management);
		okButton= (Button) findViewById(R.id.buttonOK);
		cardManagementText = (TextView) findViewById(R.id.textCardManagement);	
    	Configuration config= getResources().getConfiguration() ;
 	   	if ((config.screenWidthDp<500)||(config.screenHeightDp<500)){
 	   		cardManagementText.setTextAppearance(this, android.R.style.TextAppearance_Medium);
    	} else {
    		cardManagementText.setTextAppearance(this, android.R.style.TextAppearance_Large);    		
    	}

		okButton.setOnClickListener(new OnClickListener() {          
			public void onClick(View v) {
				((Activity)contextActivity).finish();
			}
		});
	
        checkIntent(getIntent());   

	}
    
    @Override
    protected void onNewIntent(Intent intent) {
    	// Activity defined in manifest as single instance, so when the activity is already created,
    	// new intents arrive through this method
        super.onNewIntent(intent);
        checkIntent(intent);
    }
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.menu_card_list) {
	        Intent intent = new Intent();
	        intent.setClass(contextActivity, TagListActivity.class);
	        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
	        startActivity(intent);	            		
			return true;
        }
		if (id == R.id.menu_about) {
			MainActivity.showAbout(contextActivity);
			return true;
		}

		return super.onOptionsItemSelected(item);

	}
    @Override
    protected void onResume() {
        super.onResume();
        Repository.setConfiguration(contextActivity, getString(R.string.cardManagementFlag), true);
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        Repository.setConfiguration(contextActivity, getString(R.string.cardManagementFlag), false);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
       // db.onUpgrade(db.getWritableDatabase(), 0, 0);
        db.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	Log.d(LOG_TAG, "Activity result received from Tasker select task. ResultCode: " + resultCode);
    	if (resultCode == Activity.RESULT_OK && requestCode == RfidTagDialog.TASKER_REQUEST_CODE) {
		    String taskName = data.getDataString();
		    if (taskName!=null) {
		    	RfidTagDialog.refreshTaskName(this, taskName);
		    }
		} else {
			Log.d(LOG_TAG, "Activity result received from Tasker select task. RfidTag not updated. ResultCode: " + resultCode);											
		}
    }    
    
    void checkIntent(Intent i) {
    	if (i.hasExtra(getString(R.string.TAGID_EXTRA))){
    		String tagId=i.getStringExtra(getString(R.string.TAGID_EXTRA));
    		if (tagId!=null) {
    			RfidTag tag=db.getRfidTag(tagId);
	            if (tag==null) tag= new RfidTag(tagId);
	            RfidTagDialog.showUserDialog(contextActivity, tag, null);
    		}
    	}
    	
    }

}
