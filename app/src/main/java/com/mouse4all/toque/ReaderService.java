/*
 * Copyright 2018 Mouse4all SL
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mouse4all.toque;

import com.mouse4all.toque.db.DatabaseHandler;
import com.mouse4all.toque.db.RfidTag;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;
import net.dinglisch.android.tasker.TaskerIntent;

public class ReaderService extends Service {
	
	static final String LOG_TAG = "ACR122 service";
	public final static int RESTART_NOTIFICATION_ID=123;
	public final static int WAIT_TIME_BETWEEN_CONSECUTIVE_TAGS=1000;
	private DatabaseHandler db;
	private long lastTagTimestamp;

    Context context;
    
    //Global
    BroadcastReceiver receiver = new BroadcastReceiver() {
 	   @Override
 	   public void onReceive(Context c, Intent intent) {
 	      String action = intent.getAction();
 	      Log.d(LOG_TAG, "New global Intent received in Service. Action: " + action);       	
 	      if (action==null) return;
 	      if (action.equalsIgnoreCase(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
 	      	 MainActivity.showToast(context.getString(R.string.serviceUSBUnplugged));
 	      	 Log.d(LOG_TAG, "MouseService: USB device detached");
             UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
             Log.d(LOG_TAG, "DeviceId: " + device.getDeviceId() + "  - VendorId: " + device.getVendorId());
             if ((device!=null)&&(ACR122Reader.isRfidReaderDevice(device.getVendorId()))) {
	                Log.d(LOG_TAG, "ACR122 device detached Stop reader service");
	                Intent mainMenuIntent = new Intent(context, MainActivity.class);
	      			//no Extras required because service is foreground and stopping it also removes 
	      			//the notification icon/intent
	      			mainMenuIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);	        
	  				startActivity (mainMenuIntent);  			
	      			((Service)context).stopSelf();
             } 
 	      } 	  
 	   };
    };
	
    
    //Local
	BroadcastReceiver cardEventReceiver= new BroadcastReceiver() {

		@Override
	    public void onReceive(Context context, Intent intent) {
			long currentTime= System.currentTimeMillis();
			if ((currentTime - lastTagTimestamp)< WAIT_TIME_BETWEEN_CONSECUTIVE_TAGS) {
		        Log.d(LOG_TAG, "Discarded consecutive card event.");
		        return;
			} else {
				lastTagTimestamp=currentTime;
			}
	        String action = intent.getAction();
	        String extra=intent.getStringExtra(context.getString(R.string.cardEventExtra));
	        
  	        checkNewTag(extra);
	        Log.d(LOG_TAG, "Received card event intent. Action: " + action +
	        		";  Extra: " + extra);
	    }

	};

    @Override
    public void onCreate() {
        super.onCreate();
     try {
        context=this;
        Log.d(LOG_TAG, "Create ACR122 service");
		db=new DatabaseHandler(context);
		lastTagTimestamp=System.currentTimeMillis();
        IntentFilter filter = new IntentFilter();
        filter.addAction(context.getString(R.string.cardEventServiceAction));
        LocalBroadcastManager.getInstance(this).registerReceiver(cardEventReceiver, filter);
     } catch (Exception ex) {
    	Log.d(LOG_TAG, "error on init " + ex.getMessage());
    	Toast.makeText(context, "Error creando servicio: \n" + ex.getMessage() , Toast.LENGTH_LONG).show();
     }
    } 
    
    public void checkNewTag(String id) {
    	String warningMessage=null;
    	RfidTag tag =null;
    	if (id==null) {
    		warningMessage=getResources().getString(R.string.noCardIdMessage);
    	} else {
    		tag=db.getRfidTag(id);
    		if (tag==null) {
        		warningMessage=getResources().getString(R.string.newCardIdMessage);
    		} else if (tag.getTaskName()==null) {
        		warningMessage=getResources().getString(R.string.nullTaskMessage);   			
    		}
    	}
    	if (warningMessage!=null) {
    		Log.d(LOG_TAG, "Service triggers warning message for detected tag. Id: " +id + " - " + warningMessage);
            Intent i = new Intent(context, WarningActivity.class);
		    i.putExtra(context.getString(R.string.WARNING_EXTRA),warningMessage);
		    i.putExtra(context.getString(R.string.TAGID_EXTRA), id);
		    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		    context.startActivity(i);    
    	} else {
    		Log.d(LOG_TAG, "Service triggers Tasker task for detected tag. Id: " +id + " - Task: " + tag.getTaskName());
			TaskerIntent.Status result= TaskerIntent.testStatus( this );
			int intResult =(int) result.ordinal();
	    	if ( result.equals( TaskerIntent.Status.OK ) ) {
	    		TaskerIntent i = new TaskerIntent( tag.getTaskName() );
	    		sendBroadcast( i );
	    	} else {
	    		Log.d(LOG_TAG, "Service cannot execute Tasker task: " + intResult);
	            Intent i = new Intent(context, WarningActivity.class);
	            String extendedError="";
	            if (result == TaskerIntent.Status.NotInstalled){
						extendedError=context.getString(R.string.taskerNotInstalled);
				} else if (result == TaskerIntent.Status.NoPermission){
					extendedError=context.getString(R.string.taskerNoPermission);
				} else if (result == TaskerIntent.Status.NotEnabled){
					extendedError=context.getString(R.string.taskerNotEnabled);
				} else if (result == TaskerIntent.Status.AccessBlocked){
					extendedError=context.getString(R.string.taskerNoExternalAccess);
				}
			    i.putExtra(context.getString(R.string.WARNING_EXTRA),context.getString(R.string.taskerKOMessage)+ " (Error: " + intResult + " - " + result+ ")"
				+"\n"+ extendedError);
			    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
			    context.startActivity(i);    
	    	}	    	
    	}    	
    }
    
    public void hotRestart() {
   	  resetConfig();
      initialiseConfig(); 	
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();	
        Log.d(LOG_TAG, "Destroy Reader service");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(cardEventReceiver);
        db.close();
    	MainActivity.showToast(context.getString(R.string.serviceDestroyed));
        resetConfig();
    }

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		MainActivity.showToast(context.getString(R.string.serviceCreated));
        if (intent==null){
        	Log.d(LOG_TAG, "Error Starting service with null intent");
			this.stopSelf();
			return Service.START_NOT_STICKY;
        }  
        Log.d(LOG_TAG, "Start Reader service");
        // Start as foreground service so that it has higher priority and OS does not stop it
        // This adds a notification icon  in notification bar
        Intent outputIntent = new Intent(this, MainActivity.class);
        outputIntent.setPackage(getResources().getString(R.string.package_name));
       	outputIntent.putExtra(getResources().getString(R.string.stop),true);        
       	outputIntent.putExtra(getResources().getString(R.string.service_name_reader),true);        
       	outputIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

       	PendingIntent contentIntent = PendingIntent.getActivity(this, 0, outputIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        Notification notification = new Notification.Builder(this)    	       
        .setAutoCancel(true)
        .setContentIntent(contentIntent)
        .setContentTitle(AppContainer.getInstance().getResources().getString(R.string.app_name)).setContentText(AppContainer.getInstance().getResources().getString(R.string.stop))
        .setOngoing(true)
		//.setLargeIcon(BitmapFactory.decodeResource(AppContainer.getInstance().getResources(),R.drawable.m4a_nfc))
        .setSmallIcon(R.drawable.notification_icon).build();
		startForeground(1320,notification);
		cancelRestartNotification();
		
		hotRestart();
        //boolean restart = intent.getBooleanExtra(context.getResources().getString(R.string.extra_restart), false);
		return Service.START_STICKY;	
	}
	
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }   
    
    public void resetConfig() {
    	try {
    		unregisterReceiver(receiver);
    	} catch (Exception ex) {
    		Log.d(LOG_TAG, "unregister exc "+ ex.getMessage());
    	}
    	ACR122Reader.destroyReader();
    }
    
    void initialiseConfig() {

        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(receiver,filter);
        ACR122Reader.openReader(context);      
    }  

    public static void createRestartNotification(){
	    Intent i = new Intent(AppContainer.getInstance(),MainActivity.class);
    	i.putExtra(AppContainer.getInstance().getResources().getString(R.string.extra_restart), AppContainer.getInstance().getResources().getString(R.string.extra_restart));
        PendingIntent contentIntent = PendingIntent.getActivity(AppContainer.getInstance(), 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
	    Notification notification = new Notification.Builder(AppContainer.getInstance())    	       
	    .setAutoCancel(true)
	    .setContentIntent(contentIntent)
	    .setContentTitle(AppContainer.getInstance().getResources().getString(R.string.app_name)).setContentText(AppContainer.getInstance().getResources().getString(R.string.extra_restart))
	    .setOngoing(false)
	    .setSmallIcon(R.drawable.notification_icon).build();
	    notification.flags |=Notification.FLAG_AUTO_CANCEL;
	    NotificationManager notificationManager = (NotificationManager) AppContainer.getInstance().getSystemService(NOTIFICATION_SERVICE);
	    notificationManager.notify(RESTART_NOTIFICATION_ID, notification);
    }    
    
    void cancelRestartNotification(){
	    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	    notificationManager.cancel(RESTART_NOTIFICATION_ID);
    }   
    
    void openReader() {
    	
    }

}