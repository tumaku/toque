/*
 * Copyright 2018 Mouse4all SL
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mouse4all.toque;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class WarningActivity extends Activity {
	private static final String LOG_TAG="ACR122 WarningActivity";

    Button okButton, cardManagementButton;
	TextView warningText;
	Context context;
	Handler handler;
    Runnable runnable;		
    boolean includeTagIdExtra=false;
	String tagId= null;
	static final int WARNING_DURATION =10000; //milliseconds
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context=this;
		setContentView(R.layout.activity_warning);
		okButton= (Button) findViewById(R.id.buttonOK);
		cardManagementButton= (Button) findViewById(R.id.buttonCardManagement);
		warningText = (TextView) findViewById(R.id.textWarning);

		okButton.setOnClickListener(new OnClickListener() {          
			public void onClick(View v) {
				((Activity)context).finish();
			}
		});
		
		cardManagementButton.setOnClickListener(new OnClickListener() {          
			public void onClick(View v) {
		        Intent intent = new Intent();
		        intent.setClass(context, CardManagementActivity.class);
		        if (includeTagIdExtra) {
		        	intent.putExtra(context.getString(R.string.TAGID_EXTRA), tagId);
		        }
		        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		        startActivity(intent);					
				((Activity)context).finish();
			}
		});
		
        checkIntent(getIntent());   
	}

    @Override
    protected void onNewIntent(Intent intent) {
    	// Activity defined in manifest as single instance, so when the activity is already created,
    	// new intents arrive through this method
        super.onNewIntent(intent);
        checkIntent(intent);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopHandler();
    }	   
	   
    void checkIntent(Intent i) {
    	String msg= i.getStringExtra(getString(R.string.WARNING_EXTRA));
		if (msg==null) return;
		final String warningMessage=new String(msg);
		includeTagIdExtra=i.hasExtra(getString(R.string.TAGID_EXTRA));
		tagId= i.getStringExtra(getString(R.string.TAGID_EXTRA));
    	runOnUiThread(new Runnable() {
            @Override
            public void run() {
    	    	warningText.setText(warningMessage);
    	    	if (!includeTagIdExtra) {
    	    		cardManagementButton.setVisibility (View.GONE);
    	    	} else {
    	    		cardManagementButton.setVisibility (View.VISIBLE);
    	    	}
            }
        });
    	
    	stopHandler();
		handler = new Handler(); 
    	runnable = new Runnable() { 
            public void run() { 
  	        	try{
  	        		((Activity)context).finish();
  	        	} catch (Exception ex) {
  	        		Log.d(LOG_TAG, "Error while trying to kill warning activity after timeout: " +
  	        				ex.getMessage());
  	        	}
            }
          }; 	
	    handler.postDelayed(runnable, WARNING_DURATION);   	
    }
    
    void stopHandler(){
		try {
			if (handler!=null) {
				handler.removeCallbacks(runnable);
				handler=null;
			}
		} catch (Exception e){
    	    Log.e(LOG_TAG, "Exception onDestroy handler: " + e.getMessage());    	    	
		}	    	
    }
}
