/*
 * Copyright 2018 Mouse4all SL
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mouse4all.toque;

import java.util.List;

import com.mouse4all.toque.db.RfidTag;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RfidCardAdapter extends ArrayAdapter<RfidTag>{
	private List<RfidTag> list;
	private Activity activity;
	private TextView tagIdText;
	private TextView aliasText;
	private TextView taskText;
	private LinearLayout rowLayout;

	public RfidCardAdapter(Activity activity, List<RfidTag> list){
		super(activity, 0, list);
		this.activity=activity;
		this.list=list;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {		
		if(convertView == null){
			LayoutInflater inflater=activity.getLayoutInflater();			
			convertView=inflater.inflate(R.layout.tag_list_smart_row_layout, null);
		}			
		rowLayout=(LinearLayout) convertView.findViewById(R.id.rowLayout);
		tagIdText=(TextView) convertView.findViewById(R.id.tagId);
		aliasText=(TextView) convertView.findViewById(R.id.alias);
		taskText=(TextView) convertView.findViewById(R.id.task);


		RfidTag tag=list.get(position);
		tagIdText.setText((tag.getTagId()==null)?"null":tag.getTagId());
		aliasText.setText(tag.getAlias());
		taskText.setText((tag.getTaskName()==null)?"null":tag.getTaskName());
		
		if (position%2!=0) {
			rowLayout.setBackgroundColor(Color.WHITE);
		} else {
			rowLayout.setBackgroundColor(Color.parseColor("#f5f5f5"));
		}
		
		return convertView;
	}

}
