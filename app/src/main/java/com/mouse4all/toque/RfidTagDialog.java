/*
 * Copyright 2018 Mouse4all SL
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.mouse4all.toque;

import com.mouse4all.toque.db.DatabaseHandler;
import com.mouse4all.toque.db.RfidTag;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import net.dinglisch.android.tasker.TaskerIntent;

public class RfidTagDialog {
	private static final String LOG_TAG="ACR122 RfidTagDialog";
	private static final String NULL_TASK_NAME="---";
	public static final int TASKER_REQUEST_CODE=1245625;
	private static AlertDialog dlg;
	private static TextView dialogTaskName;

	
	   protected static void refreshTaskName(Activity act, final String newName){
		   if ((dialogTaskName!=null)&&(newName!=null)) {
			   act.runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                	   dialogTaskName.setText(newName);
                   }
               });		   
		   }
	   }
	
	   protected static void showUserDialog(Context context, RfidTag tag, final ArrayAdapter<RfidTag> rfidTagAdapter) {
		   if (dlg!=null) {
			   ((Activity)context).runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
               	   try {
						 dlg.dismiss();
						 dlg=null;
						 dialogTaskName=null;

                	   } catch (Exception ex){
                	   }
                   }
               });		   
 		    }
	    	AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.dialog_light));
	    	LayoutInflater inflater = ((Activity)context).getLayoutInflater();
	        View layout=inflater.inflate(R.layout.m4a_rfid_tag_dialog,null);   
	        builder.setView(layout);

	        final Context context_=context;
	        final EditText alias= (EditText)layout.findViewById(R.id.alias);
	        final TextView tagId= (TextView)layout.findViewById(R.id.tagId);
	        final TextView taskName= (TextView)layout.findViewById(R.id.taskName);
	        final RfidTag tag_=tag;
	    	final DatabaseHandler db=new DatabaseHandler(context);
	    	dialogTaskName= taskName;
	        tagId.setText(tag_.getTagId());
	        String taskNameString= tag_.getTaskName();
	        if (taskNameString==null) taskNameString= NULL_TASK_NAME;
	        taskName.setText(taskNameString);
		    OnClickListener onClickListener =new OnClickListener() {
			   public void onClick(View v) {
				   try {
					   if (TaskerIntent.taskerInstalled(context_)) {
						   ((Activity)context_).startActivityForResult( TaskerIntent.getTaskSelectIntent(), TASKER_REQUEST_CODE );
					   } else {
						   MainActivity.showToast(context_.getString(R.string.installTasker));
						   Log.d(LOG_TAG, "Cannot start task selection activity because Tasker is not installed.");
					   }
				   } catch (Exception ex) {
					   MainActivity.showToast("Error: " + context_.getString(R.string.installTasker));
					   Log.d(LOG_TAG, "Exception: error trying to start task selection activity because Tasker is not installed: " + ex.getMessage());
				   }
			   }
		   };
		   taskName.setOnClickListener(onClickListener);
	        String aliasString= tag_.getAlias();
	        if (aliasString==null) aliasString="";
	        alias.setText(aliasString);
	        final String aliasString_=aliasString;
	        final ImageButton updateButton= (ImageButton)layout.findViewById(R.id.buttonChangeTask);
	        updateButton.setFocusable(true);
	        updateButton.setFocusableInTouchMode(true);
	        updateButton.requestFocus();
	        updateButton.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_YES);
	        updateButton.setOnClickListener(onClickListener);
	        String okTag;
	        boolean createFlag= db.getRfidTag(tag_.getTagId())==null;
	        if (createFlag) {
		    	builder.setTitle(context.getString(R.string.createTagTitle));
		    	okTag=context.getString(R.string.create);
	        } else {
		    	builder.setTitle(context.getString(R.string.updateTagTitle));
		    	okTag=context.getString(R.string.update);
		    	builder.setNeutralButton(context_.getString(R.string.delete), 
			        	new DialogInterface.OnClickListener() {
				            @Override
				            public void onClick(DialogInterface dialog, int which) {
				                new AlertDialog.Builder(context_)
				                .setTitle(context_.getString(R.string.deleteTagTitle))
				                .setMessage(context_.getString(R.string.deleteTagText) +
				                		"\n" + aliasString_ + " (" + tag_.getTagId() +")" )
				                .setNegativeButton(context_.getString(R.string.cancel),null)
				                .setPositiveButton(context_.getString(R.string.delete), 
				                	new DialogInterface.OnClickListener() {
							            @Override
							            public void onClick(DialogInterface dialog, int which) {
					            			Log.d(LOG_TAG, "Delete tag " + tag_.getTagId());	  			
							            	db.deleteRfidTag(tag_);
							            	if (rfidTagAdapter!=null) {
							            		rfidTagAdapter.remove(tag_);
							            	}
							            }
				        			})	
				                .show();
				            }
						});
	        }
	        final String okTag_=okTag;
	    	builder.setNegativeButton(context.getString(R.string.cancel), null);
	    	builder.setPositiveButton(okTag, 
		           	new DialogInterface.OnClickListener() {
			            @Override
			            public void onClick(DialogInterface dialog, int which) {
	            			Log.d(LOG_TAG, okTag_ + ": " + tag_.getTagId());
	            			tag_.setAlias(alias.getText().toString());
	            			String updatedTaskName=taskName.getText().toString();
	            			if (updatedTaskName.equalsIgnoreCase(NULL_TASK_NAME)) {
	            				updatedTaskName=null;
	            			}
	            			tag_.setTaskName(updatedTaskName);
	            			db.updateOrCreateRfidTag(tag_);
			            	if (rfidTagAdapter!=null) {
			            		rfidTagAdapter.notifyDataSetChanged();
			            	}
			            }
					});

	         dlg=builder.show();
	    }
}
