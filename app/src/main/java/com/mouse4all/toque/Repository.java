/*
 * Copyright 2018 Mouse4all SL
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mouse4all.toque;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


public class Repository {
	final static String LOG_TAG="ACR122 Repository";
	
	private static SharedPreferences getPrefs(Context context) { 
		return PreferenceManager.getDefaultSharedPreferences(context);
	}
 
	public static boolean setConfiguration(Context context, String property, boolean state) {
		SharedPreferences prefs = getPrefs(context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(property, state);
		editor.commit();  	  
		Log.d(LOG_TAG, "Updated " + property +" flag: " + Boolean.toString(state));
		return (state);
	}
	
	public static  boolean getConfiguration(Context context, String property) {
		SharedPreferences prefs = getPrefs(context);
		try {
			return prefs.getBoolean(property, false);
		} catch (Exception ex) {
			return false;
		}
	}
}
