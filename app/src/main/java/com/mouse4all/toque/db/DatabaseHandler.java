package com.mouse4all.toque.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHandler extends SQLiteOpenHelper {
 
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "rfidManager";
 
    // RfidTags table name
    private static final String TABLE_RFIDTAGS = "rfidTags";
 
    // RfidTags Table Columns names
    private static final String KEY_TAGID = "tagId";
    private static final String KEY_ALIAS = "alias";
    private static final String KEY_TASKNAME = "taskName";
 
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_RFIDTAGS_TABLE = "CREATE TABLE " + TABLE_RFIDTAGS + "("
                + KEY_TAGID + " TEXT PRIMARY KEY," 
                + KEY_ALIAS + " TEXT, " + KEY_TASKNAME + " TEXT)";
        db.execSQL(CREATE_RFIDTAGS_TABLE);
    }
 
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RFIDTAGS);
        onCreate(db);
    }

    public void addRfidTag(RfidTag rfidTag) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_TAGID, rfidTag.getTagId()); 
        values.put(KEY_ALIAS, rfidTag.getAlias()); 
        values.put(KEY_TASKNAME, rfidTag.getTaskName()); 
 
        db.insert(TABLE_RFIDTAGS, null, values);
        db.close(); 
    }
 
    public RfidTag getRfidTag(String tagId) {
        SQLiteDatabase db = this.getReadableDatabase();
 
        Cursor cursor = db.query(TABLE_RFIDTAGS, new String[] { KEY_TAGID, KEY_ALIAS, KEY_TASKNAME },
        		KEY_TAGID + "=?",
                new String[] { tagId }, null, null, null, null);
        if (cursor.moveToFirst()) {
            return new RfidTag(cursor.getString(0), cursor.getString(1), cursor.getString(2));
        } else {
        	return null;
        }
    }
    
    // Getting All Contacts
    public List<RfidTag> getAllRfidTags() {
        List<RfidTag> rfidTagList = new ArrayList<RfidTag>();
        String selectQuery = "SELECT  * FROM " + TABLE_RFIDTAGS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
            	rfidTagList.add(new RfidTag(cursor.getString(0), cursor.getString(1), cursor.getString(2)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return rfidTagList;
    }
 
    public int updateRfidTag(RfidTag rfidTag) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TAGID, rfidTag.getTagId());
        values.put(KEY_ALIAS, rfidTag.getAlias());
        values.put(KEY_TASKNAME, rfidTag.getTaskName());
        int result= db.update(TABLE_RFIDTAGS, values, KEY_TAGID + " = ?",
                new String[] { rfidTag.getTagId() });
        db.close();
        return result;
    }
    
    public void updateOrCreateRfidTag(RfidTag rfidTag) {
    	RfidTag newTag= getRfidTag(rfidTag.getTagId());
     	if (newTag!=null){
     		updateRfidTag(rfidTag);
     	} else {
     		addRfidTag(rfidTag);   		
     	}
    }
 
    public void deleteRfidTag(RfidTag rfidTag) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RFIDTAGS, KEY_TAGID + " = ?",
                new String[] { rfidTag.getTagId() });
        db.close();
    }
 
    public void deleteRfidTag(String tagId) {
    	deleteRfidTag(new RfidTag(tagId));
    }
 
    public int getRfidTagsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_RFIDTAGS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int result= cursor.getCount();
        cursor.close();
        return result;
    }
 
}

