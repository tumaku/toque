package com.mouse4all.toque.db;

public class RfidTag {
    String _tagId;
    String _taskName;
    String _alias;
     
    public RfidTag(String tagId, String alias, String taskName){
        this._tagId = tagId;
        if (alias==null) alias="";
        this._alias= alias;
        this._taskName = taskName;
    }
     
    public RfidTag(String tagId){
        this._tagId = tagId;
        this._alias= "";
        this._taskName = null;
    }
    
    public String getTagId(){
        return this._tagId;
    }
     
    public void setTagId(String tagId){
        this._tagId = tagId;
    }
 
    public String getAlias(){
        return this._alias;
    }
     
    public void setAlias(String alias){
        this._alias = alias;
    }
    
    public String getTaskName(){
        return this._taskName;
    }
     
    public void setTaskName(String taskName){
        this._taskName = taskName;
    }
    
    @Override
    public String toString() {
    	String tagS;
    	tagS=(getTagId()==null)?"null":getTagId();
    	String taskS;
    	
    	taskS=(getTaskName()==null)?"null":getTaskName();
    	return (tagS+ " ("+ getAlias() + ") - " + taskS);
    }
    
}
